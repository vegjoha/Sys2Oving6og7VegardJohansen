package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/message/")
public class AService {
    private static String placeholderText = "No information found ";
    private static ServerManager serverManagerObject = new ServerManager();
    private static ArrayList<User> userList = new ArrayList<User>();
    private static User defaultUser = new User("defaultuser");
    private static UserManager userManagerObject = new UserManager(userList);
    
    @GET
    @Path("/getName/{name}")//Should give webpage String text with question, numbered answers and deadline time.
    @Produces(MediaType.APPLICATION_JSON)
    public String getInfo(@PathParam("name") String name) {  //changed from getKunde() 
        String returnString = "";
        if (name.equals("getAllInfo")){
            returnString = userManagerObject.getAllUserScoreInfo();
        }
        serverManagerObject.singleServersListOfQuestions.get(serverManagerObject.getServer(name)).getCurrentQuestion();

        if (name.equals("getAllServerNames")){
            returnString = serverManagerObject.getNamesOfallServers();
        }
        
        return returnString;
    }
    
    @GET
    @Path("/score_board")
    @Produces(MediaType.APPLICATION_JSON)
    public String getScoreBoard() {
        String scoreBoardinfo = userManagerObject.getAllUserScoreInfo();
        
        
        
        
        
        return scoreBoardinfo;
        //return "does this display?";
    }
    
    @GET
    @Path("/server/start_time/{userNameClient}")
    @Produces(MediaType.APPLICATION_JSON)
    public long getServerStartTime(@PathParam("userNameClient") String userNameClient) {
        String serverUserIsUsing = userManagerObject.userList.get(
            userManagerObject.getUser(userNameClient)).currentServerName;
        long stringToReturn = serverManagerObject.singleServersListOfQuestions.get(
            serverManagerObject.getServer(serverUserIsUsing)).getServerStartTime();
        return stringToReturn;
    }
    
    @GET
    @Path("/server/round_time/{userNameClient}")
    @Produces(MediaType.APPLICATION_JSON)
    public long getServerRoundTime(@PathParam("userNameClient") String userNameClient) {
        String serverUserIsUsing = userManagerObject.userList.get(
            userManagerObject.getUser(userNameClient)).currentServerName;
        long stringToReturn = serverManagerObject.singleServersListOfQuestions.get(
            serverManagerObject.getServer(serverUserIsUsing)).getTimePerQuestion();
        return stringToReturn;
    }
    
    @GET
    @Path("/server/match_round_info/{userNameClient}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getServerMatchRoundInfoForClient(@PathParam("userNameClient") String userNameClient) {
        //String scoreBoardinfo = userManagerObject.getAllUserScoreInfo();
        
        String serverUserIsUsing = userManagerObject.userList.get(
            userManagerObject.getUser(userNameClient)).currentServerName;
        
        String stringToReturn = serverManagerObject.singleServersListOfQuestions.get(
            serverManagerObject.getServer(userNameClient)).getCurrentQuestion().getAllInfo();
        
        
        //return scoreBoardinfo;
        return stringToReturn;
        //getServer(String theServerName)
        // getUser(String userName)
    }
    
    //GET SCORE BOARD INFO
    
    
    //more posts
    
    //clan up code! its baaaaad have fun doing it right and  well :)
    
    //rest/message/server_browser
    
    @PUT
    @Path("/log_in_user")
    @Consumes(MediaType.APPLICATION_JSON) //SetMessageParameters
    public void loginUser(String name){//, String serverName){
        //String name = "QwQ";
        //String serverName = "QwQ";
        int userNum = userManagerObject.getUser(name);
        if (userNum <= -1){
            //make new user for username
            User newUser = new User(name);
            userManagerObject.userList.add(newUser);

            userNum = userManagerObject.getUser(name);
            userManagerObject.userList.get(userNum).setCurrentServerName("1");
        } else {
            userManagerObject.userList.get(userNum).name = name;
            //userManagerObject.userList.add(name);
            //userManagerObject.userList.get(userNum).setCurrentServerName(serverName);
        }
        
    }
    /*
    @POST
    @Path("/log_in_user2")
    @Consumes(MediaType.APPLICATION_JSON) //SetMessageParameters
    public void loginNewUser(String name){//String name, String serverName){
        //String name = "QwQ";
        //String serverName = "QwQ";
        int userNum = userManagerObject.getUser(name);
        if (userNum <= -1){
            //make new user for username
            User newUser = new User(name);
            userManagerObject.userList.add(newUser);

            userNum = userManagerObject.getUser(name);
            //userManagerObject.userList.get(userNum).setCurrentServerName(serverName);
        } else {

            //userManagerObject.userList.get(userNum).setCurrentServerName(serverName);
        }
        
    }
    */
    
    
    
    
    @POST
    @Path("/server_browser")//{nameServer}")
    @Consumes(MediaType.APPLICATION_JSON) //SetMessageParameters
    public void setServerBrowser(SetMessageParametersServer SMPS){//, 
    //public void setServerBrowser(@PathParam("nameServer") String nameUser,
    //        @PathParam("nameServer") String nameServer){//, //@PathParam("nameUser") 
            //@PathParam("nameServer") String nameServer){
            
            //String nameServer = "1";
            //String nameUser, String nameServer){
        
        //SMPsb.messageTekstFromUser;
        
        int userNum = userManagerObject.getUser(SMPS.name);
        if (userNum <= -1){
            //make new user for username
            User newUser = new User(SMPS.name);
            userManagerObject.userList.add(newUser);

            userNum = userManagerObject.getUser(SMPS.name);
            userManagerObject.userList.get(userNum).setCurrentServerName(SMPS.serverName);
        } else {

            userManagerObject.userList.get(userNum).setCurrentServerName(SMPS.serverName);
        }
        
    }
    
    @POST
    @Path("/set_message")
    @Consumes(MediaType.APPLICATION_JSON) //SetMessageParameters
    public void setMessage(SetMessageParameters SMP){
            
            /*String operationType, String name, String number, String timePerQuestion, 
            String question, String answer1, String answer2, String  answer3, String answer4,
            String answerScore1, String answerScore2, String answerScore3, String answerScore4) {*/
            
            
        Date newDate = new Date();    
        
        int newNumber = Integer.parseInt(SMP.number);
        int newTimePerQuestion = Integer.parseInt(SMP.timePerQuestion); // can be either answer picked or time per question depending on operationType
        
        if (Integer.parseInt(SMP.operationType) == 1){ //add question picked by user and the time it was picked 
            int userNum = userManagerObject.getUser(SMP.name);
            if (userNum <= -1){
                //make new user for username
                User newUser = new User(SMP.name);
                userManagerObject.userList.add(newUser);
                
                userNum = userManagerObject.getUser(SMP.name);
                userManagerObject.userList.get(userNum).setAnswerNumberPicked(newNumber, newTimePerQuestion);
            } else {
                
                userManagerObject.userList.get(userNum).setAnswerNumberPicked(newNumber, newTimePerQuestion);
            }
            //userManagerObject.userList.get(userManagerObject.getUser(SMP.name)).setAnswerNumberPicked(newNumber, newTimePerQuestion);
        }
        
        if (Integer.parseInt(SMP.operationType) == 2){ //Add question to single server, name = serverName
            int newAnswerScore1 = Integer.parseInt(SMP.answerScore1);
            int newAnswerScore2 = Integer.parseInt(SMP.answerScore2);
            int newAnswerScore3 = Integer.parseInt(SMP.answerScore3);
            int newAnswerScore4 = Integer.parseInt(SMP.answerScore4);
            SingleAnswer newSingleAnswer1 = new SingleAnswer(SMP.answer1, 1, newAnswerScore1);
            SingleAnswer newSingleAnswer2 = new SingleAnswer(SMP.answer1, 1, newAnswerScore2);
            SingleAnswer newSingleAnswer3 = new SingleAnswer(SMP.answer1, 1, newAnswerScore3);
            SingleAnswer newSingleAnswer4 = new SingleAnswer(SMP.answer1, 1, newAnswerScore4);
            
            ArrayList<SingleAnswer> newAnswerList = new ArrayList<SingleAnswer>();            
            newAnswerList.add(newSingleAnswer1);
            newAnswerList.add(newSingleAnswer2);
            newAnswerList.add(newSingleAnswer3);
            newAnswerList.add(newSingleAnswer4);
            
            SingleQuestion newSingleQuestion = new SingleQuestion(newAnswerList, SMP.question, 
                    (serverManagerObject.singleServersListOfQuestions.get(serverManagerObject.getServer(SMP.name)).getnumberOfQuestions() + 1) );
            
            serverManagerObject.singleServersListOfQuestions.get(serverManagerObject.getServer(SMP.name)).addSingleQuestion(newSingleQuestion);
        }
        
        if (Integer.parseInt(SMP.operationType) == 3){ //Add server, name = serverName
            int newAnswerScore1 = Integer.parseInt(SMP.answerScore1);
            int newAnswerScore2 = Integer.parseInt(SMP.answerScore2);
            int newAnswerScore3 = Integer.parseInt(SMP.answerScore3);
            int newAnswerScore4 = Integer.parseInt(SMP.answerScore4);
            SingleAnswer newSingleAnswer1 = new SingleAnswer(SMP.answer1, 1, newAnswerScore1);
            SingleAnswer newSingleAnswer2 = new SingleAnswer(SMP.answer1, 1, newAnswerScore2);
            SingleAnswer newSingleAnswer3 = new SingleAnswer(SMP.answer1, 1, newAnswerScore3);
            SingleAnswer newSingleAnswer4 = new SingleAnswer(SMP.answer1, 1, newAnswerScore4);
            
            ArrayList<SingleAnswer> newAnswerList = new ArrayList<SingleAnswer>();            
            newAnswerList.add(newSingleAnswer1);
            newAnswerList.add(newSingleAnswer2);
            newAnswerList.add(newSingleAnswer3);
            newAnswerList.add(newSingleAnswer4);
            
            SingleQuestion newSingleQuestion = new SingleQuestion(newAnswerList, SMP.question, 1);  
            ArrayList<SingleQuestion> newQuestionList = new ArrayList<SingleQuestion>(); 
            newQuestionList.add(newSingleQuestion);
            Server newServer = new Server(SMP.name, newQuestionList, newTimePerQuestion, newDate.getTime());
            serverManagerObject.addServer(newServer);
        }
        
        if (Integer.parseInt(SMP.operationType) == 5){ //Delete server, name = serverName
            serverManagerObject.deleteServer(SMP.name);
            userManagerObject.resetServerUsersPicked(SMP.name);
        }
        
        if (Integer.parseInt(SMP.operationType) == 4){ //Check the answers picked by users in server name and add points
                                                   //Answer number correct is for correct answer is answerScore2
                                                   //Answer Score for correct answer is answerScore2
                         
            SingleQuestion newSingleQuestion = serverManagerObject.singleServersListOfQuestions.get(serverManagerObject.getServer(SMP.name)).getCurrentQuestion();  
            userManagerObject.checkIfAswersIsCorrect(newSingleQuestion, SMP.name);         
        }
    }
    
    @POST
    @Path("/VotingScreen/{answerNumber}/{userName}/{timeAnswerMade}")
    @Consumes(MediaType.APPLICATION_JSON) //SetMessageParameters
    public void userPickAnswer(@PathParam("answerNumber") String answerNumber,
            @PathParam("userName") String userName,
            @PathParam("timeAnswerMade") String timeAnswerMade){
        
        long timeAnswerMadeLong = Long.parseLong(timeAnswerMade);
        int newAnswerNumber = Integer.parseInt(answerNumber);
        SingleQuestion newSingleQuestion = serverManagerObject.singleServersListOfQuestions.get(serverManagerObject.getServer(userName)).getCurrentQuestion();  
        userManagerObject.checkIfAswersIsCorrect(newSingleQuestion, userName); 
         
    }
    //setServerBrowser(@PathParam("name") String nameUser, 
    //        @PathParam("name") String nameServer)
    
}
