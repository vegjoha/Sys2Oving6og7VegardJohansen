package services;

import java.util.Date;


public class User {
    
    String name;
    String currentServerName;
    
    long lastMatchEnd;
    Date currentDate;
    int score;
    int scoreTotal;
    int answerNumberPicked;
    long dateForQuestionDeadline;

    public User() {
    }
    public User(String newNavn) {
        this.name = newNavn;
    }
    public String getNavn(){
        return name;
    }
    public void setNavn(String newNavn){
        name = newNavn;
    }
    public String getCurrentServerName(){
        return currentServerName;
    }
    public void setCurrentServerName(String newCurrentServerName){
        currentServerName = newCurrentServerName;
    }
    
    public void setLastMatchEnd(long newLastMatchEnd){
        lastMatchEnd = newLastMatchEnd;
    }
    
    public void setAnswerNumberPicked(int num, long newDateForQuestionDeadline){
        answerNumberPicked = num;
        dateForQuestionDeadline = newDateForQuestionDeadline;
    }
    
    public int getAnswerNumberPicked(){
        if (currentDate.getTime() >= dateForQuestionDeadline){
            return -1;
        } else {
            return answerNumberPicked;
        }
    }
    
    public int getScore(){
        long currentTime = currentDate.getTime();
        if (lastMatchEnd < currentTime) {
            score = 0;
        }
        return score;
    }
    
    public void setMoreScore(int moreScore){
        score =+ moreScore;
        scoreTotal =+ moreScore;
    }
    
    
    
}
