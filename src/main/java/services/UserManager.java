/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.ArrayList;

/**
 *
 * @author Vegard
 */
public class UserManager {
    
    ArrayList<User> userList = new ArrayList<User>();
    
    
    
    public UserManager() {
    }
    public UserManager(ArrayList<User> newUserManager) {
        this.userList = newUserManager;
        userList.add(new User("0"));
    }
    
    public int getUser(String userName){
        //if (userList == null){
        for(int i = 0; i < userList.size(); i++){
            if (userList.get(i).name.equals(userName)){
                return i;
            }
        }
        //}
        return -1;
    }
    
    public void resetQuestionUsersPicked(String serverName){
        for(int i = 0; i < userList.size(); i++){
            if (userList.get(i).currentServerName.equals(serverName)){
                userList.get(i).answerNumberPicked = -1;
            }
        }
    }
    
    public void resetServerUsersPicked(String serverName){
        for(int i = 0; i < userList.size(); i++){
            if (userList.get(i).currentServerName.equals(serverName)){
                userList.get(i).currentServerName = "none";
            }
        }
    }
    
    public void checkIfAswersIsCorrect(SingleQuestion newSingleQuestion, String serverName) {
        //(String newSingleQuestion, int NewNumber, int newScoreNumber)
        //newSingleQuestion.answerList.get(i).answerCorrect;
        //newSingleQuestion.answerList.size();
        for(int j = 0; j < newSingleQuestion.answerList.size(); j++){
            for(int i = 0; i < userList.size(); i++){
                if (userList.get(i).currentServerName.equals(serverName)){
                    if (userList.get(i).answerNumberPicked == newSingleQuestion.answerList.get(j).number){
                        userList.get(i).score =+ newSingleQuestion.answerList.get(j).scoreNumber;
                        userList.get(i).scoreTotal =+ newSingleQuestion.answerList.get(j).scoreNumber;
                    }
                }
            }
        }
    }
    
    public String getAllUserScoreInfo(){
        String allInfo = "";
            for(int i = 0; i < userList.size(); i++){
                
                if(userList.get(i).name != null){
                    allInfo += " Name: " + userList.get(i).name + " ";
                } else {
                    allInfo += " Name: " + "_nuffin_here_m8_" + " ";
                }
                if(userList.get(i).currentServerName != null){
                    allInfo += " currentServerName: " + userList.get(i).currentServerName + "/n";
                } else {
                    allInfo += " currentServerName: " + "_nuffin_here_m8_" + "/n";
                }
                if(userList.get(i).name != null){
                    allInfo += " score in current server: " + userList.get(i).score + " ";
                } else {
                    allInfo += " score in current server: " + "_nuffin_here_m8_" + " ";
                }
                allInfo += " scoreTotal: " + userList.get(i).scoreTotal + "/n";
            }
        return allInfo;
    }
    
}
