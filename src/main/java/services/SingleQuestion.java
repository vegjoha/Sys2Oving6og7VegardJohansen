/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.ArrayList;

/**
 *
 * @author Vegard
 */
public class SingleQuestion {
    String questionText;
    String serverName;
    int questionNumber;
    
    ArrayList<SingleAnswer> answerList;
    
    public SingleQuestion() {
    }
    public SingleQuestion(ArrayList<SingleAnswer> newAnswerList, String newQuestionText, int newQuestionNumber) {
        this.answerList = newAnswerList;
        this.questionText = newQuestionText;
        this.questionNumber = newQuestionNumber;
    }
    public String getQuestionText(){
        return questionText;
    }
    
    public void setQuestionText(String newQuestionText){
        questionText = newQuestionText;
    }
    
    public void addAnswer(SingleAnswer newAnswer){
        answerList.add(newAnswer);
    }
    
    public String getAllInfo(){
        String allInfo = "Server Name: " + serverName + "/n" + "Question text: " +
            questionText + "/n" + "Questions: /n";
        for(int i = 0; i < answerList.size(); i++){
            allInfo += answerList.get(i) + "/n";
        }
        return allInfo;
    }
    
    
    

}
