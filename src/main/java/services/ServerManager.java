/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Vegard
 */
public class ServerManager {
    
    ArrayList<Server> singleServersListOfQuestions = new ArrayList<Server>();
    Date date = new Date();
    
    public ServerManager() {
    }
    public ServerManager(ArrayList<Server> newListOfQuestions,  Date newDate) {   //String newId,
        this.singleServersListOfQuestions = newListOfQuestions;
        //this.serverName = NewServerName;
        this.date = newDate;
        
    }
    public ArrayList<Server> getServerManager(){
        return singleServersListOfQuestions;
    }
    
    public void setServerManager(ArrayList<Server> newSingleQuestion){
        singleServersListOfQuestions = newSingleQuestion;
    }
    
    public void addServer(Server newServer){
        newServer.dateOfTestStart = date.getTime();
        singleServersListOfQuestions.add(newServer);
        
    }
    
    public void deleteServer(String serverName){
        for(int i = 0; i < singleServersListOfQuestions.size(); i++){
            if (singleServersListOfQuestions.get(i).serverName.equals(serverName)) {
                
                singleServersListOfQuestions.remove(i);
            }
        }
    }
    
    public int getServer(String theServerName){
        for(int i = 0; i < singleServersListOfQuestions.size(); i++){
            if (singleServersListOfQuestions.get(i).serverName.equals(theServerName)){
                return i;
            }
        }
        return -1;
    }
    
    public String getNamesOfallServers() {
        String allServerNames = "";
        for(int i = 0; i < singleServersListOfQuestions.size(); i++){
            allServerNames += singleServersListOfQuestions.get(i).serverName + " " + "/n";
        }
        return allServerNames;
    }
    
    
    
    
    
    //Make add new SingleQuestion
    //Make delete SingleQuestion
}
