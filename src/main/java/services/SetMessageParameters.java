/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

/**
 *
 * @author Vegard
 */
public class SetMessageParameters {
    String operationType;
    String name;
    String number;
    String timePerQuestion;
    
    String question;
    String answer1;
    String answer2;
    String answer3;
    String answer4;

    String answerScore1;
    String answerScore2;

    String answerScore3;
    String answerScore4;
    
    public SetMessageParameters(){   
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTimePerQuestion() {
        return timePerQuestion;
    }

    public void setTimePerQuestion(String timePerQuestion) {
        this.timePerQuestion = timePerQuestion;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public String getAnswerScore1() {
        return answerScore1;
    }

    public void setAnswerScore1(String answerScore1) {
        this.answerScore1 = answerScore1;
    }

    public String getAnswerScore2() {
        return answerScore2;
    }

    public void setAnswerScore2(String answerScore2) {
        this.answerScore2 = answerScore2;
    }

    public String getAnswerScore3() {
        return answerScore3;
    }

    public void setAnswerScore3(String answerScore3) {
        this.answerScore3 = answerScore3;
    }

    public String getAnswerScore4() {
        return answerScore4;
    }

    public void setAnswerScore4(String answerScore4) {
        this.answerScore4 = answerScore4;
    }
    
    
    
    
    
    
    
}
