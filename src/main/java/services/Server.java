package services;

import java.util.ArrayList;
import java.util.Date;


public class Server {
    
    String navn;
    Date date;
    long dateOfTestStart;
    long timePerQuestion;
    String serverName;
    ArrayList<SingleQuestion> listOfQuestions;
    long serverStartTime;
    
    public Server() {
    }
    public Server(String NewServerName, ArrayList<SingleQuestion> newListOfQuestions,
            long newTimePerQuestion, long newServerStartTime) {   //String newId,
        this.listOfQuestions = newListOfQuestions;
        this.serverName = NewServerName;
        this.timePerQuestion = newTimePerQuestion;
        this.serverStartTime = newServerStartTime;
        
    }
    public ArrayList<SingleQuestion> getServer(){
        return listOfQuestions;
    }
    
    public void setServer(ArrayList<SingleQuestion> newQuestionList){
        listOfQuestions = newQuestionList;
    }
    
    public void addSingleQuestion(SingleQuestion newSingleQuestion){
        listOfQuestions.add(newSingleQuestion);
    }
    
    public int getnumberOfQuestions(){
        return listOfQuestions.size();
    }

    public long getTimePerQuestion() {
        return timePerQuestion;
    }

    public void setTimePerQuestion(long timePerQuestion) {
        this.timePerQuestion = timePerQuestion;
    }

    public long getServerStartTime() {
        return serverStartTime;
    }

    public void setServerStartTime(long serverStartTime) {
        this.serverStartTime = serverStartTime;
    }
    
    public SingleQuestion getCurrentQuestion(){
        /*
        if(date.getTime() >= ((listOfQuestions.size() + 1) * timePerQuestion)){
            return null;
        }*/
        
        int i = (int) (((date.getTime() - dateOfTestStart) / timePerQuestion)+1);
        return listOfQuestions.get(i); //might be buggy
    }
    
    
    
    
    
    
}
