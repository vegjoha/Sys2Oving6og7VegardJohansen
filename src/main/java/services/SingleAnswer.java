package services;

public class SingleAnswer {
    String singleAnswer;
    String id;
    int number;
    int scoreNumber; //from 0 to 100 (where 0 is false and 1 to 100 is amount of points for correct)
    String serverName;
    
    public SingleAnswer() {
    }
    public SingleAnswer(String newSingleQuestion, int NewNumber, int newScoreNumber) {
        this.singleAnswer = newSingleQuestion;
        this.number = NewNumber;
        this.scoreNumber = newScoreNumber;
    }
    public String getServerDataSingleQuestion(){
        return singleAnswer;
    }
    
    public void setServerDataSingleQuestion(String newSingleQuestion){
        singleAnswer = newSingleQuestion;
    }
}
